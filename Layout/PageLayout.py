#https://blog.51cto.com/u_15080020/4553873
#kivy八种布局：FloatLayout、BoxLayout、AnchorLayout、GridLayout、PageLayout、RelativeLayout、ScatterLayout、StackLayout。

#PageLayout：与其它布局不司，这是个多页动态布局。此布局可以在窗口内创建多个页面的布局，这些页面可以翻转，每个页面子部件均可作为单独的窗口页面进行开发。

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.pagelayout import PageLayout
class PageLayoutWidget(PageLayout):
    def __init__(self,**kwargs):
        super(PageLayoutWidget, self).__init__(**kwargs)
        bt0=Button(text='bt0',background_color=[.3,.9,.3,1])
        bt1=Button(text='bt1',background_color=[.9,.3,.3,1])
        self.add_widget(bt0)
        self.add_widget(bt1)
class PageApp(App):
    def build(self):
        return PageLayoutWidget()
if __name__ =='__main__':
    PageApp().run()