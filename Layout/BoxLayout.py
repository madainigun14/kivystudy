#https://blog.51cto.com/u_15080020/4553873
#kivy八种布局：FloatLayout、BoxLayout、AnchorLayout、GridLayout、PageLayout、RelativeLayout、ScatterLayout、StackLayout。

#BoxLayout：盒子布局，是可以将子部件水平或垂直进行排列的布局，类似Android中的线性布局，如果不设置任何大小，子部件将会以10px的间距平分父窗口的大小。

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.graphics import Rectangle,Color
 
class BoxLayoutWidget(BoxLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)
 
        with self.canvas:
            Color(1,1,1,1)
            self.rect=Rectangle(pos=self.pos,size=self.size)
            self.bind(pos=self.update_rect,size=self.update_rect)
 
        self.add_widget(Button(text='hello'))
        self.add_widget(Button(text='BoxLayout'))
 
    def update_rect(self,*args):
        #设置背景尺寸，可忽略
        self.rect.pos=self.pos
        self.rect.size=self.size
 
class BoxApp(App):
    def build(self):
        return BoxLayoutWidget()
 
if __name__ =='__main__':
    BoxApp().run()