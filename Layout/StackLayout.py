#https://blog.51cto.com/u_15080020/4553873
#kivy八种布局：FloatLayout、BoxLayout、AnchorLayout、GridLayout、PageLayout、RelativeLayout、ScatterLayout、StackLayout。

#StackLayout：堆栈布局，在此布局中，可以进行垂直或水平的排列子部件，并且各个小部件可以不必相同，排列的方向由orientation属性进行指定。

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.stacklayout import StackLayout
from kivy.graphics import Rectangle,Color
 
class StackLayoutWidget(StackLayout):
    def __init__(self,**kwargs):
        super(StackLayoutWidget, self).__init__(**kwargs)
 
        with self.canvas:
            Color(1,1,1,1)
            self.rect=Rectangle(pos=self.pos,size=self.size)
            self.bind(pos=self.update_rect,size=self.update_rect)
 
        #遍历添加按钮
        for i in range(25):
            btn=Button(text=str(i),width=40+i*5,size_hint=(None,0.15))
            self.add_widget(btn)
 
    def update_rect(self,*args):
        self.rect.pos=self.pos
        self.rect.size=self.size
 
class StackApp(App):
    def build(self):
        return StackLayoutWidget()
 
if __name__ =="__main__":
    StackApp().run()