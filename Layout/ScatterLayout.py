#https://blog.51cto.com/u_15080020/4553873
#kivy八种布局：FloatLayout、BoxLayout、AnchorLayout、GridLayout、PageLayout、RelativeLayout、ScatterLayout、StackLayout。

#ScatterLayout：分散布局，与RelativeLayout类似。当布局更改位置时，布局内的小部件也会跟着父布局一起变动，并且子部件的位置及大小会相对于父布局自动调整，并且此布局还可以进行平移、旋转、缩放布局。

from kivy.app import App
from kivy.uix.image import AsyncImage
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scatterlayout import ScatterLayout
from kivy.graphics import Rectangle,Color
 
class ScatterLayoutWidget(ScatterLayout):
    pass
 
class BoxLayoutWidget(BoxLayout):
        def __init__(self,**kwargs):
            super(BoxLayoutWidget, self).__init__(**kwargs)
 
            with self.canvas:
                Color(1,1,1,1)
                self.rect=Rectangle(pos=self.pos,size=self.size)
                self.bind(pos=self.update_rect,size=self.update_rect)
 
                #创建一个ScatterLayout布局
            scatter_layout=ScatterLayoutWidget()
            #异步加载图片
            image=AsyncImage(source='https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png') #http://sck.rjkflm.com/images/logo1.png
            #将图片添加到ScatterLayout布局中
            scatter_layout.add_widget(image)
            #将ScatterLayout布局嵌套在BoxLayout布局中
            self.add_widget(scatter_layout)
 
        def update_rect(self,*args):
            #设置背景尺寸，可忽略
            self.rect.pos=self.pos
            self.rect.size=self.size
 
 
 
class ScatterApp(App):
    def build(self):
        return BoxLayoutWidget()
 
if __name__ =='__main__':
    ScatterApp().run()
