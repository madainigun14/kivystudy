#https://blog.51cto.com/u_15080020/4553873
#kivy八种布局：FloatLayout、BoxLayout、AnchorLayout、GridLayout、PageLayout、RelativeLayout、ScatterLayout、StackLayout。

#GridLayout：网格布局，使用此布局可以将子部件排列成多行多列的矩阵布局。当设定了列数cols或者行数rows后，子部件大小尺寸与子部件个数多少发生变化时，此布局会根据该值进行扩展，但不会超过界限值。

from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.graphics import Rectangle,Color
 
class GridLayoutWidget(GridLayout):
    def __init__(self,**kwargs):
        super(GridLayoutWidget, self).__init__(**kwargs)
 
        with self.canvas:
            Color(1,1,1,1)
            self.rect=Rectangle(pos=self.pos,size=self.size)
            self.bind(pos=self.update_rect,size=self.update_rect)
 
        self.padding = 20
        self.spacing = 20
 
        self.cols=3
        for i in range(6):
            btn=Button(text=str(i),background_color=[0,1,1,1],size_hint=[.3,.2])
 
            self.add_widget(btn)
 
    def update_rect(self,*args):
        self.rect.pos=self.pos
        self.rect.size=self.size
 
class GridApp(App):
    def build(self):
        return GridLayoutWidget()
 
if __name__ == '__main__':
    GridApp().run()